import colors from 'vuetify/es5/util/colors'

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - زد بوت',
    htmlAttrs: {
      lang: 'ar'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
      
      { hid: 'title', name: 'title', content: 'منصة زد بوت' },
      { hid: 'og:title', name: 'og:title', content: 'منصة زد بوت' },

      { hid: 'description', name: 'description', content: 'منصة تسعى لتسهيل عملية الحصول على الملفات الدراسية الخاصة بالجامعات عن طريقة مشاركة الملفات على المنصة من جهة طلاب او مدرسين المقررات , ومن ثم الوصول اليها من طرف الطلاب عن طريق لوحة تحكم كتطبيق ويب او بوت تلغرام لسهولة الاستخدم , مع ضرورة بداء الاشتراك من طرف بوت التلغرام وذلك تسهيلاً لعملية التسجيل في النظام' },
      { hid: 'og:description', name: 'og:description', content: 'منصة تسعى لتسهيل عملية الحصول على الملفات الدراسية الخاصة بالجامعات عن طريقة مشاركة الملفات على المنصة من جهة طلاب او مدرسين المقررات , ومن ثم الوصول اليها من طرف الطلاب عن طريق لوحة تحكم كتطبيق ويب او بوت تلغرام لسهولة الاستخدم , مع ضرورة بداء الاشتراك من طرف بوت التلغرام وذلك تسهيلاً لعملية التسجيل في النظام' },
      { hid: 'og:image', name: 'og:image', content: 'https://zdbots.com/suLogo.png' },
//https://zdbots.com/suLogo.png
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'theme-color', content: '#8683f3' },
      { name: 'msapplication-TileColor', content: '#8683f3' },
      { name: 'fapple-mobile-web-app-status-bar-style', content: '#8683f3' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com', crossorigin: true },
      { href: 'https://fonts.googleapis.com/css2?family=Tajawal:wght@500&display=swap', rel: 'stylesheet' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],
  loadingIndicator: {
    name: 'cube-grid',
    color: '#8683f3',
    background: 'white'
  },
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/myTheme.js',
    '~/plugins/tool.js',
    '~/plugins/utilte.js',
    '~/plugins/rand.js',
    '~/plugins/http.js'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify'
  ],
  eslint: {
    fix: true
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth-next'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: 'https://api-su.zdbots.com/api/'
  },
  env: {
    baseUrl: 'https://api-su.zdbots.com/'
  },
  auth: {
    strategies: {
      local: {
        token: {
          property: 'data.token'
          // required: true,
          // type: 'Bearer'
        },
        user: {
          property: 'data'
          // autoFetch: true
        },
        endpoints: {
          login: { url: '/login', method: 'post' },
          user: { url: '/me', method: 'get' },
          logout: false
        }
      }
    },
    redirect: {
      login: '/auth/login',
      logout: '/',
      home: '/'
    }
  },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    rtl: true,
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          error: '#ff6868',
          success: '#00cba9',
          primary: '#8683f3'//colors.blue.lighten1,
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: true
  }
}
