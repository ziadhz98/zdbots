export default function ({ store, redirect }) {
  if (store.state.auth.user.type === 'client') {
    return redirect('/')
  }
}
