export default function ({ store, redirect }) {
    if (store.state.auth.user.first_login) {
      return redirect('/auth/newPassword')
    }
  }