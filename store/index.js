export const state = () => {
  return {
    itemsIsCut: JSON.parse(localStorage.getItem('cutItems')) || { folder: [], file: [] }
  }
}

export const getters = {
  isAuthenticated (state) {
    return state.auth.loggedIn
  },
  loggedInUser (state) {
    return state.auth.user
  },

  itemsIsCut (state) {
    return state.itemsIsCut
  }
}

export const mutations = {
  SET_CUT (store, payload) {
    store.itemsIsCut = payload
  }
}
