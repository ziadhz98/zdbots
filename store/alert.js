export const state = () => ({
  alert: {
    status: false,
    msg: 'تمت العملية بنجاح',
    color: 'success',
    icon: null,
    timeout: 6000
  }
})

export const mutations = {
  setAlert (state, alert) {
    state.alert = alert
  },
  setState (state) {
    state.alert.status = false
  }
}

export const getters = {
  Alert (state) {
    return state.alert
  }
}
