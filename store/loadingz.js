export const state = () => ({
  loading: false,
  message: 'لحظة من فضلك',
  color: 'primary',
  ms: 0
})

export const mutations = {
  loadDialog (state, loading) {
    state.loading = loading.status
    state.message = loading.message
    state.color = loading.color
  },
  loadPage(state , ms){
    state.ms = ms
  }
}

export const getters = {
  load (state) {
    return state.loading
  },
  loadPage (state) {
    return state.ms
  },
  message (state) {
    return state.message
  },
  color (state) {
    return state.color
  }
}
