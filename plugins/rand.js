import Vue from 'vue'

Vue.mixin({
  methods: {
    randId () {
      return ('ziad-' + Math.floor(Math.random() * Date.now().toString()))
    },
    timeoutz (ms) {
      return new Promise(resolve => setTimeout(resolve, ms))
    }
  }
})
