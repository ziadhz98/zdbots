import Vue from 'vue'

Vue.mixin({
  data () {
    return {
      // Form
      textInput: {
        filled: true,
        dense: true,
        rounded: true
      },

      // Btn
      btn: {
        block: true, rounded: true, class: 'shadowz'
      }
    }
  }
})
