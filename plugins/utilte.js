
export default ({ app, store }, inject) => {
  inject('ziad', {
    // Loading
    loading ({ status = false, message = 'لحظة من فضلك', color = 'primary' }) {
      store.commit('loadingz/loadDialog', { status, message, color })
    },
    timeout (ms) {
      return new Promise(resolve => setTimeout(resolve, ms))
    },
    async loadingPage (ms) {
      store.commit('loadingz/loadPage', 1)
      await this.timeout(ms)
      store.commit('loadingz/loadPage', 0)
    },
    // Alert
    alert ({
      status = true,
      msg = 'تمت العملية بنجاح',
      color = 'success',
      icon = null,
      timeout = 6000
    }) {
      store.commit('alert/setAlert', { status, msg, color, icon, timeout })
    }
  })
}
