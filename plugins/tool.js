import Vue from 'vue'
import { mapGetters } from 'vuex'
Vue.mixin({
  data () {
    return {
      // validation
      required: [
        v => !!v || 'this is required'
      ],
      min6Char: [
        v => (v && v.length >= 6) || 'يجيب ان يكون من 6 احرف على الاقل'
      ]
    }
  },
  computed: {
    ...mapGetters({ user: 'loggedInUser' })
  }
})
