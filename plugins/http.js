import Vue from 'vue'

Vue.mixin({
  data () {
    return {
      baseUrl: process.env.baseUrl
    }
  },
  methods: {
    async post (url, data, redirect = false) {
      this.$ziad.loading({ status: true })
      try {
        const d = await this.$axios.post(url, data)
        this.$ziad.alert({ status: true })
        if (redirect) { this.$router.push(redirect) }
        return d.data.data
      } catch (e) {
        this.$ziad.alert({ status: true, msg: e.response.data.msg || 'error', color: 'error' })
        throw e.message
      } finally {
        this.$ziad.loading({ status: false })
      }
    },
    async get (url) {
      this.$ziad.loading({ status: true })
      try {
        const res = await this.$axios.get(url)
        return res.data.data
      } catch (e) {
        this.$ziad.alert({ status: true, msg: 'Error', color: 'error' })
        throw e.message
      } finally {
        this.$ziad.loading({ status: false })
      }
    },
    async uploadPhoto (file, type) {
      this.$ziad.loading({ status: true })
      try {
        const data = new FormData()
        data.append('image', file)
        data.append('type', type)
        const res = await this.$axios.post('/tool/uploadImage', data)
        return res.data.data
      } catch (e) {
        this.$ziad.alert({ status: true, msg: 'Error Upload', color: 'error' })
        throw e.message
      } finally {
        this.$ziad.loading({ status: false })
      }
    }
  }
})
